# flutter_challange

Test Flutter from Infosys,
create simple splash screen with login validation

## Getting Started
Requirement
- Flutter 3.0.1
- Dart 2.17

## Dependencies
- intl: ^0.17.0 for localization
- get: ^4.6.5 for Route management

## How To Run
- Make sure you have fulfilled that requirement
- Connect physical device or open emulator
- run flutter pub get -> for getting the dependencies
- run flutter run --release
 
